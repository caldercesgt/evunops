-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 28-01-2021 a las 01:08:44
-- Versión del servidor: 5.7.24
-- Versión de PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `unops`
--

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista01`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vista01` (
`tot_incidentes` bigint(21)
,`HorasSoporte` double
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista02`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vista02` (
`id` bigint(20) unsigned
,`asunto` varchar(500)
,`fecha_ingreso` date
,`fecha_resolucion` date
,`horas_soporte` double
,`resolucion` varchar(500)
,`user_id` bigint(20) unsigned
,`clasificaciones_id` bigint(20) unsigned
,`prioridad_id` bigint(20) unsigned
,`estado_id` bigint(20) unsigned
,`perfil_id` bigint(20) unsigned
,`created_at` timestamp
,`updated_at` timestamp
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista03`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vista03` (
`perfil_id` bigint(20) unsigned
,`prioridad_id` bigint(20) unsigned
,`mes` varchar(7)
,`COUNT(*)` bigint(21)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista04`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vista04` (
`perfil_id` bigint(20) unsigned
,`COUNT(*)` bigint(21)
);

-- --------------------------------------------------------

--
-- Estructura para la vista `vista01`
--
DROP TABLE IF EXISTS `vista01`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista01`  AS  select count(0) AS `tot_incidentes`,sum(`incidentes`.`horas_soporte`) AS `HorasSoporte` from `incidentes` group by `incidentes`.`clasificaciones_id` ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista02`
--
DROP TABLE IF EXISTS `vista02`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista02`  AS  select `incidentes`.`id` AS `id`,`incidentes`.`asunto` AS `asunto`,`incidentes`.`fecha_ingreso` AS `fecha_ingreso`,`incidentes`.`fecha_resolucion` AS `fecha_resolucion`,`incidentes`.`horas_soporte` AS `horas_soporte`,`incidentes`.`resolucion` AS `resolucion`,`incidentes`.`user_id` AS `user_id`,`incidentes`.`clasificaciones_id` AS `clasificaciones_id`,`incidentes`.`prioridad_id` AS `prioridad_id`,`incidentes`.`estado_id` AS `estado_id`,`incidentes`.`perfil_id` AS `perfil_id`,`incidentes`.`created_at` AS `created_at`,`incidentes`.`updated_at` AS `updated_at` from `incidentes` where (`incidentes`.`estado_id` = 1) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista03`
--
DROP TABLE IF EXISTS `vista03`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista03`  AS  select `incidentes`.`perfil_id` AS `perfil_id`,`incidentes`.`prioridad_id` AS `prioridad_id`,date_format(`incidentes`.`fecha_ingreso`,'%Y-%m') AS `mes`,count(0) AS `COUNT(*)` from `incidentes` group by `incidentes`.`perfil_id`,`incidentes`.`prioridad_id`,date_format(`incidentes`.`fecha_ingreso`,'%Y-%m') ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista04`
--
DROP TABLE IF EXISTS `vista04`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista04`  AS  select `incidentes`.`perfil_id` AS `perfil_id`,count(0) AS `COUNT(*)` from `incidentes` group by `incidentes`.`perfil_id` having (count(0) > 10) ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
